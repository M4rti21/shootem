using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Timeline.Actions;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Windows;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset p_InputAsset;
    public static InputActionAsset p_Input;

    private enum SwitchMachineStates { NONE, IDLE, WALK, RUN, ATTACK, COMBO, JUMP, DEAD };
    private SwitchMachineStates m_CurrentState;
    private Rigidbody2D m_RigidBody;
    private InputAction m_Movement;
    private Animator m_Animator;
    private Transform m_Transform;

    [SerializeField] private HitBoxController m_HitBox;
    [SerializeField] private int m_Damage;
    [SerializeField] private int m_DamageCombo;

    private Boolean isRotated;
    private Boolean isCombo;

    [SerializeField] private int JumpForce = 5;

    [SerializeField] private int m_Speed = 3;

    [SerializeField] private float m_AttackRate = 0.25f;

    private void Awake()
    {
        p_Input = Instantiate(p_InputAsset);
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Animator = GetComponent<Animator>();
        m_Transform = GetComponent<Transform>();
        m_Movement = p_Input.FindActionMap("gameplay").FindAction("Move");
        InputActionMap gameplay = p_Input.FindActionMap("gameplay");
        gameplay.FindAction("Run").started += StartRun;
        gameplay.FindAction("Run").canceled += EndRun;
        gameplay.FindAction("Attack").started += StartAttack;
        gameplay.FindAction("Jump").started += StartJump;
        gameplay.Enable();
        isRotated = false;
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    private void Update()
    {
        UpdateState();
    }

    private void OnDestroy()
    {
        p_Input.FindActionMap("gameplay").Disable();
    }

    private void StartCombo()
    {
        isCombo = true;
    }

    private void EndCombo()
    {
        isCombo = false;
    }

    private void StartRun(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.RUN);
    }

    private void EndRun(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.WALK);
    }

    private void StartAttack(InputAction.CallbackContext context)
    {
        if (m_CurrentState == SwitchMachineStates.ATTACK) return;
        ChangeState(SwitchMachineStates.ATTACK);
    }

    private void EndAttack()
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    private void StartJump(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.JUMP);
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("IdleAnimation");

                break;

            case SwitchMachineStates.WALK:

                m_Animator.Play("WalkAnimation");

                break;

            case SwitchMachineStates.RUN:

                m_Animator.Play("RunAnimation");

                break;

            case SwitchMachineStates.ATTACK:

                if (isCombo)
                {
                    m_Animator.Play("ComboAttackAnimation");
                    m_HitBox.SetDmg(m_DamageCombo);
                    return;
                }
                m_Animator.Play("AttackAnimation");
                m_HitBox.SetDmg(m_Damage);

                break;
            case SwitchMachineStates.JUMP:
                m_Animator.Play("JumpAnimation");
                m_RigidBody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
                break;

            case SwitchMachineStates.DEAD:

                m_Animator.Play("DeadAnimation");

                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.JUMP:
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                if (m_Movement.ReadValue<Vector2>().x != 0)
                    ChangeState(SwitchMachineStates.WALK);

                break;
            case SwitchMachineStates.WALK:

                Move(m_Speed);

                break;
            case SwitchMachineStates.RUN:

                Move(m_Speed * 2);

                break;
            case SwitchMachineStates.ATTACK:

                break;

            case SwitchMachineStates.JUMP:

                break;
            case SwitchMachineStates.DEAD:

                break;

            default:
                break;
        }
    }

    private void Move(float spd)
    {
        m_RigidBody.velocity = Vector2.right * m_Movement.ReadValue<Vector2>().x * spd;

        if (m_RigidBody.velocity.x == 0)
        {
            ChangeState(SwitchMachineStates.IDLE);
            return;
        }

        if (m_Movement.ReadValue<Vector2>().x < 0 && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (m_Movement.ReadValue<Vector2>().x > 0 && isRotated)
        {
            m_Transform.Rotate(0, -180, 0);
            isRotated = false;
        }
    }
}
