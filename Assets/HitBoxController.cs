using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxController : MonoBehaviour
{
    private int dmg;

    public void SetDmg(int dmg)
    {
        this.dmg = dmg;
    }

    public int getDmg()
    {
        return dmg;
    }
}
