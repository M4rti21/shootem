using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using static UnityEngine.GraphicsBuffer;

public class Enemy2Controller : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, PATROL, IDLE, ATTACK, DEAD };

    //---BODY---//
    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;
    private Transform m_Transform;

    //---STATE---//
    private SwitchMachineStates m_CurrentState;
    private Boolean isRotated;

    //---STATS---//
    [SerializeField] private int m_Speed = 2;
    [SerializeField] private float m_AttackRate = 2f;
    [SerializeField] private int m_Cooldown = 2;

    //---AREAS---//
    [SerializeField] private EnemyTriggerBoxController m_AttackArea;

    //---WAYPOINTS---//
    [SerializeField] private List<GameObject> m_Waypoints;
    private int index;

    private Transform m_Target;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Transform = GetComponent<Transform>();
        m_Animator = GetComponent<Animator>();
        isRotated = false;
        m_Target = null;
        m_AttackArea.OnEnter += StartAttack;
        m_AttackArea.OnExit += StopAttack;
        index = 0;
    }
    private void Start()
    {
        InitState(SwitchMachineStates.PATROL);
    }
    private void Update()
    {
        UpdateState();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Attack")) return;
        Destroy(this.gameObject);
    }

    public void StartAttack(Transform target)
    {
        m_Target = target;
        TurnAround(target.position);
        ChangeState(SwitchMachineStates.ATTACK);
    }

    public void StopAttack(Transform target)
    {
        m_Target = null;
        ChangeState(SwitchMachineStates.IDLE);
    }
   
    private void NextWaypoint() {
        if (index + 1 >= m_Waypoints.Count) {
            index = 0;
            return;
        }
        index++;
    }

    private void Patrol() {
        if (!m_Waypoints[index]) return;
        Vector3 me = m_Transform.position;
        Vector3 waypoint = m_Waypoints[index].transform.position;
        if (Vector3.Distance(me, waypoint) < .1f) NextWaypoint();
        Move(waypoint);
    }

    public void Move(Vector2 target)
    {
        Vector2 me = m_Transform.position;

        m_RigidBody.velocity = (target - me).normalized * m_Speed;
        TurnAround(target);
    }

    private void TurnAround(Vector2 target)
    {
        if (target.x < m_Transform.position.x && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (target.x > m_Transform.position.x && isRotated)
        {
            m_Transform.Rotate(0, -180, 0);
            isRotated = false;
        }
    }

    IEnumerator StopIdle()
    {
        ChangeState(SwitchMachineStates.PATROL);
        yield return new WaitForSeconds(m_Cooldown);
    }
    private void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.PATROL:
                m_Animator.Play("WalkAnimation");
                break;
            case SwitchMachineStates.IDLE:
                m_Animator.Play("IdleAnimation");
                StartCoroutine(StopIdle());
                break;
            case SwitchMachineStates.ATTACK:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("AttackAnimation");
                break;
            case SwitchMachineStates.DEAD:
                m_Animator.Play("DeadAnimation");
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.PATROL:
                Patrol();
                break;
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.PATROL:
                break;
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

}
