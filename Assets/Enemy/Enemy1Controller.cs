using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Enemy1Controller : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, IDLE, WALK, ATTACK, DEAD };

    //---BODY---//
    private Rigidbody2D m_RigidBody;
    private Animator m_Animator;
    private Transform m_Transform;

    //---STATE---//
    private SwitchMachineStates m_CurrentState;
    private Boolean isRotated;

    //---STATS---//
    [SerializeField] private int m_Speed = 2;
    [SerializeField] private float m_AttackRate = 2f;

    //---AREAS---//
    [SerializeField] private EnemyTriggerBoxController m_WalkArea;
    [SerializeField] private EnemyTriggerBoxController m_AttackArea;

    private Transform m_Target;

    private void Awake()
    {
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_Transform = GetComponent<Transform>();
        m_Animator = GetComponent<Animator>();
        isRotated = false;
        m_Target = null;
        m_WalkArea.OnEnter += SetTarget;
        m_WalkArea.OnExit += DelTarget;
        m_AttackArea.OnEnter += StartAttack;
        m_AttackArea.OnExit += StopAttack;
    }
    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }
    private void Update()
    {
        UpdateState();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Attack")) return;
        Destroy(this.gameObject);
    }

    public void SetTarget(Transform target)
    {
        m_Target = target;
        ChangeState(SwitchMachineStates.WALK);
    }

    public void DelTarget(Transform target)
    {
        ChangeState(SwitchMachineStates.IDLE);
        m_Target = null;
    }

    public void StartAttack(Transform target)
    {
        ChangeState(SwitchMachineStates.ATTACK);
    }

    public void StopAttack(Transform target)
    {
        ChangeState(SwitchMachineStates.WALK);
    }
   
    public void Move()
    {
        if (!m_Target) return;

        Vector2 target = m_Target.position;
        Vector2 me = m_Transform.position;
        m_RigidBody.velocity = (target - me).normalized * m_Speed;

        TurnAround(target);

    }

    private void TurnAround(Vector2 target)
    {
        if (target.x < m_Transform.position.x && !isRotated)
        {
            m_Transform.Rotate(0, 180, 0);
            isRotated = true;
            return;
        }

        if (target.x > m_Transform.position.x && isRotated)
        {
            m_Transform.Rotate(0, -180, 0);
            isRotated = false;
        }
    }

    private void ChangeState(SwitchMachineStates newState)
    {
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("IdleAnimation");
                break;
            case SwitchMachineStates.WALK:
                m_Animator.Play("WalkAnimation");
                break;
            case SwitchMachineStates.ATTACK:
                m_RigidBody.velocity = Vector2.zero;
                m_Animator.Play("AttackAnimation");
                break;
            case SwitchMachineStates.DEAD:
                m_Animator.Play("DeadAnimation");
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                Move();
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.WALK:
                break;
            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

}
