using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTriggerBoxController : MonoBehaviour
{
    public event Action<Transform> OnEnter;
    public event Action<Transform> OnExit;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;
        OnEnter?.Invoke(collision.gameObject.transform);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.CompareTag("Player")) return;
        OnExit?.Invoke(collision.gameObject.transform);
    }
}
